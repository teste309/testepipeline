#!/bin/bash

if [ -f target/sonar/report-task.txt ] 
	then 
		if [sed -n 's/^ceTaskUrl=//p' target/sonar/report-task.txt | curl | jq .task.status == "SUCCESS" ]
			exit 0
		else
			echo "Verifique " exit 1 
		fi
else
  mvn $MAVEN_CLI_OPTS sonar:sonar -Dsonar.host.url=${SONAR_HOST_URL}
fi
